const express = require('express');
const app = express();

// Set the environment variable to production to remove all the debug messages displayed
app.set("env", "production");
app.set("useHttps", true);
// app.set("useHttps", false);

// modules
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fs = require('fs');

if (app.get('useHttps')) {
    // Default location, adjust the paths accordingly to your architecture
    var cert = fs.readFileSync('/etc/letsencrypt/live/hbouffier.info/fullchain.pem');
    var key = fs.readFileSync('/etc/letsencrypt/live/hbouffier.info/privkey.pem');
    var creds = {
        key: key,
        cert: cert
    }

    var httpsServer = require('https').createServer(creds, app);
}

const httpServer = require('http').Server(app);

// routes
const index = require('./routes/index');
const cv = require('./routes/cv');

// config
const config = JSON.parse(fs.readFileSync(__dirname + '/config/config.json'));
const db = JSON.parse(fs.readFileSync(__dirname + '/projects/db.json'));

// modules
const dbUtils = require(__dirname + '/modules/dbManagement.js')
const pjTools = require(__dirname + '/modules/pjTools.js')


if (app.get('useHttps')) {
  var io = require('socket.io')(httpsServer);
}else {
  var io = require('socket.io')(httpServer);
}

// Unused
/*
// session
const sharedsession = require('express-socket.io-session');
const session = require('express-session');
*/

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//app.use(favicon(path.join(__dirname, 'public', 'img', 'favicon', 'favicon.ico')));
app.use(logger('common'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/cv', cv);

// Unused
/*
app.use(session({
    "secret": config.secrets.sessionSecret,
    "expires": new Date(Date.now() + 7 * 24 * 60 * 60 * 1000), // ONE WEEK
    "resave": cookieConfig.resave,
    "saveUninitialized": cookieConfig.saveUninitialized,
    "cookie": {
        "secure": cookieConfig.cookie.secure,
        "expires": new Date(Date.now() + 7 * 24 * 60 * 60 * 1000)
    }
}));
*/


// TODO
// MOVE IT BEFORE GOING LIVE
// TRIGGER IT ONCE EVERY X
//dbUtils.updateProjectsDb();

io.on('connection', (socket) => {
  socket.on("fetchReadme", (pjName) => {
    let readmeContent = pjTools.fetchReadme(pjName);

    socket.emit("readmeContent", readmeContent);
  });

  socket.on("fetchLanguageList", (type, mode) => {
    let languageList = pjTools.listProjects(type, mode);

    socket.emit("listProjects", languageList, mode);
  });

  socket.on("fetchThemeList", (type, mode) => {
    let themeList = pjTools.listProjects(type, mode);

    socket.emit("listProjects", themeList, mode);
  });
}); // END OF IO.CONNECTION

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

httpServer.listen(config.server.http.port) // 1600

if (app.get('useHttps')) {
    httpsServer.listen(config.server.https.port) // 4443
}
