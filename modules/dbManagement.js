const fs = require('fs');
const { join } = require("path");

const fsTools = require(__dirname + '/fsTools.js')

const config = JSON.parse(fs.readFileSync(__dirname + '/../config/config.json'));
const dbPath = '/../' + config.dbPath;

let db = JSON.parse(fs.readFileSync(__dirname + dbPath));


function updateProjectsDb() {
    let directoryListing = fsTools.listFolders("/../projects");

    let projectInfo;
    let projects = [];

    for (project of directoryListing) {
        projectInfo = fsTools.readJson(project + "/info.json");;
        projects.push(projectInfo);
    }

    writeDb("projects", projects);
}

function fetchCache() {
    return db;
}

function addProject(project) {
    if (project.type === undefined) {
        console.log("Missing project argument type", project);
        return;
    } else if (project.name === undefined) {
        console.log("Missing project argument name", project);
        return;
    } else if (Object.keys(myObject).length > 2) {
        console.log("Too much project argument(s)", project);
        return;
    } else if (Object.keys(myObject).length < 2) {
        console.log("Not enough project argument(s)", project);
        return;
    }

    let dbProjects = db.projects;

    dbProjects.push(project);fsTools

    let buf = JSON.stringify(dbProjects);

    writeDb("projects", buf);
}

function writeDb(column, data) {
    db[column] = data;

    let jsonString = JSON.stringify(db);

    fs.writeFile(join(__dirname, dbPath), jsonString, (err) => {
        if (err) {
            throw err;
        }
        console.log("Db updated", data);
    });
}

function readDb(column) {
    if ( !(db.hasOwnProperty(column)) ) {
        return null;
    }

    return db[column];
}

module.exports = {
    writeDb,
    readDb,
    fetchCache,
    addProject,
    updateProjectsDb
};
