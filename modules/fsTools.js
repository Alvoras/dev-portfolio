const fs = require('fs');
const { join } = require("path");

function listFolders(directoryPath) {
    const isDirectory = (dir) => fs.lstatSync(dir).isDirectory();

    let rawDirectoryListing = fs.readdirSync(__dirname + directoryPath);

    let directoryListing = rawDirectoryListing.map((name) => join(__dirname, directoryPath, name)).filter(isDirectory);

    return directoryListing;
}

function readJson(filePath) {
    return JSON.parse(fs.readFileSync(filePath));
}

module.exports = {
    listFolders,
    readJson
};
