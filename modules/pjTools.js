const fs = require('fs');
const {
    join
} = require("path");

const cache = require(__dirname + '/dbManagement.js');
const fsTools = require(__dirname + '/fsTools.js');

function gatherThemeInfo(projectsInfo) {
    let themesInfo = [];
    let themeNames = [];
    let themeQty = [];
    let i = 0;

    // For each language in projectsInfo
    // Then for each theme within this language
    // Verify if we already added this theme
    // If not then add theme to the name array and to the returned array and set quantity to 1
    // If yes then add 1 to quantity
    for (type in projectsInfo) {
        for (theme of projectsInfo[type].theme) {
            themeIndexOf = themeNames.indexOf(theme);

            if (themeIndexOf === -1) {
                themeNames.push(theme);
                themesInfo[i] = [];
                themesInfo[i]["name"] = theme;
                themesInfo[i]["qty"] = 1;
                i++;
            } else {
                themesInfo[themeIndexOf]["qty"]++;
            }
        }
    }

    return themesInfo;
}

function fetchProjectsType() {
  // TODO
  // Modify function in order to fetch info form cache instead of directory listing
  // see fetchProjectsTheme() below
    let projectFolders = fsTools.listFolders("/../projects");
    let projectInfo;
    let projectType;
    let types = {};

    for (project of projectFolders) {
        projectInfo = fsTools.readJson(project + "/info.json");
        projectType = projectInfo.type;

        if (types.hasOwnProperty(projectType)) {
            types[projectType].qty++;
            types[projectType].name.push(projectInfo.name);
            types[projectType].theme.push(projectInfo.theme);
            types[projectType].description.push(projectInfo.description);
            types[projectType].date.push(projectInfo.date);
        } else {
            types[projectType] = {};
            types[projectType].qty = 1;
            types[projectType].name = [];
            types[projectType].name[0] = projectInfo.name;
            types[projectType].theme = [];
            types[projectType].theme[0] = projectInfo.theme;
            types[projectType].description = [];
            types[projectType].description[0] = projectInfo.description;
            types[projectType].date = [];
            types[projectType].date[0] = projectInfo.date;
        }
    }

    return types;
}

function fetchProjectsTheme() {
  let db = cache.fetchCache();

  let themes = {};

  for (project of db.projects) {
    if (themes.hasOwnProperty(project.theme)) {
        themes[project.theme].qty++;
        themes[project.theme].name.push(project.name);
        themes[project.theme].theme.push(project.theme);
        themes[project.theme].description.push(project.description);
        themes[project.theme].date.push(project.date);
        themes[project.theme].language.push(project.type);
    } else {
        themes[project.theme] = {};
        themes[project.theme].qty = 1;
        themes[project.theme].name = [];
        themes[project.theme].name[0] = project.name;
        themes[project.theme].theme = [];
        themes[project.theme].theme[0] = project.theme;
        themes[project.theme].description = [];
        themes[project.theme].description[0] = project.description;
        themes[project.theme].date = [];
        themes[project.theme].date[0] = project.date;
        themes[project.theme].language = [];
        themes[project.theme].language[0] = project.type;
    }
  }

  return themes;
}

function listProjects(type, mode) {
  let projets;
  switch (mode) {
    case "theme":
      projects = fetchProjectsTheme();
      break;
    case "language":
      projects = fetchProjectsType();
      break;
    default:
      return;
  }

    return projects[type];
}

function fetchReadme(pjName) {
    // Never trust user input
    let projectFolders = fsTools.listFolders("/../projects");

    let pass = false;

    let candidatePjName;

    for (folder of projectFolders) {
        // Get the last item of the path string
        candidatePjName = folder.split('/').pop();

        if (candidatePjName === pjName) {
            pass = true;
        }
    }

    if (!(pass)) {
        return -1;
    }

    // If the pjName argument is legitimate, then try to fetch the README file
    let readme = fs.readFileSync(join(__dirname, "/../projects/", pjName, "/README.md"), 'utf-8');

    return readme;
}

module.exports = {
    gatherThemeInfo,
    fetchProjectsType,
    fetchProjectsTheme,
    fetchReadme,
    listProjects
};
