# ESGIUnfair
------
ESGIUnfair est un jeu de boss rush 2D réalisé en C. Entièrement configurable via des fichiers de configurations, il possède 6 niveaux différents et 2 modes de jeu : "Aventure" et "Arcade".

### Démonstration
------
<video controls>
  <source src="videos/ESGIUnfair/esgiunfair_demo.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>
