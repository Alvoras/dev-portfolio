# Fantomask
Fantomask est un utilitaire qui calcule l'adresse réseau et l'adresse de diffusion à partir de l'adresse et du mask CIDR donnée.
>
### Démonstration
------
<script src="https://asciinema.org/a/wI1KFyVVcSOfHsdGw4T4h25It.js" id="asciicast-wI1KFyVVcSOfHsdGw4T4h25It" async></script>
