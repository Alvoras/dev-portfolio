# KerbSSH
------
KerbSSH est un serveur programmé en Python 2.7 et utilisant la librairie Paramiko pour la gestion du protocole SSH-2. En plus des protocoles d'authentification classiques (login/mot de passe, clef RSA), ce serveur permet de s'authentifier via un ticket Kerberos présent dans le cache de la machine.
>
Il est déployable facilement dans un domaine Active Directory grâce au paquet d'installation MSI et est lancé automatiquement au démarrage (configurable à l'installation) sous la forme d'une service Windows.
>
Ce protocole ne gérant que l'authentification, un système de contrôle d'accès par liste (noire et blanche) a été intégré.
>
Des listes de contrôles sont également disponibles pour gérer les commandes disponibles aux utilisateurs.
>
Une interface en ligne de commande permet d'interagir avec le fichier de configuration : il est possible de modifier, visualiser et remettre à zéro la valeur d'une clef.
>
## Important
**Accès**  
   Le port choisi doit être ouvert (par défaut : 2200) et accessible (si le trafic est routé jusqu'à la machine, le port doit être accessible à travers le routeur)
   La machine doit être connectée à Internet lors de l'installation
>
**Compatibilité Kerberos**  
   L’hôte et le client doivent faire parti d’un même domaine Active Directory et le client doit s'être authentifié auprès d’un serveur Kerberos à l’ouverture de la session. La machine sur laquelle tourne le serveur doit pouvoir communiquer avec le KDC.
>
**Compatibilité Paramiko**  
   Le paquet python « gssapi » ne doit pas être installé.
>
### Démonstration
------
- CLI
<script src="https://asciinema.org/a/tjEFOgpaYah1CnVkYsQ9dBWc0.js" id="asciicast-tjEFOgpaYah1CnVkYsQ9dBWc0" async></script>

- Utilisateurs

<video controls>
  <source src="videos/KerbSSH/blacklist_cmd.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>

<video controls>
  <source src="videos/KerbSSH/kerbssh_blacklist.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>

- Commandes  
  
<video controls>
  <source src="videos/KerbSSH/kerbssh_whitelist.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>
