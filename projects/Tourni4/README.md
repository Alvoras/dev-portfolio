# Tourni 4
------
Projet d'algorithmie, le tourni 4 est un puissance 4 avec une petite particularité : il peut effectuer une rotation à 90, 180 ou 270 degrés. Il contient également un mode `Mayhem`, comprenant des jetons particuliers et des évènements, ainsi qu'un mode `Tetris`, où le but est d'empiler des jetons pour marquer des points jusqu'à être le premier à atteindre la valeur de victoire.
>
### Démonstration
------
- Classique avec rotation :
<script src="https://asciinema.org/a/MZzZ8D5HrYLyGxqYsOXISZXDo.js" id="asciicast-MZzZ8D5HrYLyGxqYsOXISZXDo" async></script>

- Mayhem :
<script src="https://asciinema.org/a/gFOePWvmMFGxTvM57wdOGsGml.js" id="asciicast-gFOePWvmMFGxTvM57wdOGsGml" async></script>

- Tetris :
<script src="https://asciinema.org/a/YrZCQd8FIM9zRkf3lM9c6GnS9.js" id="asciicast-YrZCQd8FIM9zRkf3lM9c6GnS9" async></script>
