function displayReadme(readmeContent) {
	let params = {
		html: true,
		breaks: false,
		typographer: true
	};

	let md = window.markdownit(params);

	let mdReadme = md.render(readmeContent);

	$("#readme").html(mdReadme);
}

function resetReadmePanel() {
	$("#readme").html("Aucun projet selectionné");
}

function removeSelectedButton() {
	$(".selected-button").removeClass("selected-button");
}

function removeSelectedType() {
	$(".selected-type").removeClass("selected-type");
}

function removeSelectedProject() {
	$(".selected-project").removeClass("selected-project");
}

function setProjectListTile(name) {
	$("#chosen-project-name").html(name);
}

function displayProjectList(list, mode) {
	let i = 0;
	let tiles = [];
	let currentTile = "";

	let caption;

	for (i = 0; i < list.qty; i++) {
		caption = (mode === "theme")?list.language[i]:list.theme[i];
		currentTile = '<div class="project-container" data-project="' + list.name[i] + '">\
                        <div class="project-content">\
                            <div class="project-header flex-center-row">\
                                <h3 class="project-name flex-center-row">' + list.name[i] + ' <span class="project-theme">' + caption + '</span></h3>\
                                <h6 class="project-date">' + list.date[i] + '</h6>\
                            </div>\
                            <h5 class="project-description">' + list.description[i] + '</h5>\
                        </div>\
                    </div>';

		tiles.push(currentTile);
	}

  $("#projects-list").html(tiles);
}
