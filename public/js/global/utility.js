function fetchReadme(pjName) {
    socket.emit("fetchReadme", pjName);
}

function fetchProjectList(type, mode) {
    switch (mode) {
      case "theme":
        socket.emit("fetchThemeList", type, mode);
        break;
      case "language":
          socket.emit("fetchLanguageList", type, mode);
          break;
      default:
          return;
    }
}

function getContainer(e) {
    let target = $(e.target);
    let container;

    if (target.hasClass("project-container")) {
        container = target;
    } else if (target.hasClass("project-type-container")) {
        container = target;
    } else if ((container = target.parents(".project-container")).length > 0) {
        // Store the return value while doing the the comparison in order to avoid a second ".parents" jquery call
    } else if ((container = target.parents(".project-type-container")).length > 0) {
        // Store the return value while doing the the comparison in order to avoid a second ".parents" jquery call
    }else {
        container = null;
    }

    return container;
}

function getFirstType(el) {
    let projectContainer = $("#projects-type-"+el.data("sort")+"-list");
    let firstTypeContainer = projectContainer.find(".project-type-container")[0];

    return $(firstTypeContainer);
}
