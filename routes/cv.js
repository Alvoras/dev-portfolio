const express = require('express');
const router = express.Router();
const { join } = require('path');

const { createReadStream } = require('fs');

router.get('/', (req, res, next) => {
  let filepath = "public/cv/CV_HadrienBouffier-2018.pdf";
	let stream = createReadStream(filepath);

  stream.pipe(res);
});

module.exports = router;
