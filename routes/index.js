const express = require('express');
const router = express.Router();

const fs = require('fs');
const config = JSON.parse(fs.readFileSync(__dirname+'/../config/config.json'));

const pjTools = require(__dirname + '/../modules/pjTools.js')

/* GET front page. */
router.get('/', (req, res, next) => {
    const projectsTypeInfo = pjTools.fetchProjectsType();
    const projectsThemeInfo = pjTools.fetchProjectsTheme();
    //const themesInfo = pjTools.gatherThemeInfo(projectsInfo);
    const firstTheme = Object.keys(projectsThemeInfo)[0];
    const firstLanguageKey = Object.keys(projectsTypeInfo)[0];
    const firstType = projectsTypeInfo[firstLanguageKey];

    res.render('index', {
        title: config.title,
        projectsTypeInfo,
        projectsThemeInfo,
        firstTheme,
        firstLanguageKey,
        firstType
    });
});

module.exports = router;
